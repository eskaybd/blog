---
title: আল-আমিন
search: 'user://data/persist/static/blog.full.js'
theme:
    style: izamal
sitemap:
    changefreq: daily
menu: Blog
twitterenable: true
twittercardoptions: summary
facebookenable: true
content:
    items: '@self.children'
    limit: 5
    order:
        by: date
        dir: desc
    pagination: true
    url_taxonomy_filters: true
feed:
    description: 'শখের বাংলা ব্লগ'
pagination: true
limit: 5
---

আমার শখের **বাংলা** ব্লগ