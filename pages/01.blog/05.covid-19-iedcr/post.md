---
title: 'COVID 19 ও IEDCR সম্পর্কে যা যেনে রাখা ভালো'
media_order: covid-19.jpg
published: true
date: '22-04-2020 01:24'
publish_date: '22-04-2020 01:24'
metadata:
    'COVID 19, কোভিড ১৯': 'ব্লগ '
taxonomy:
    category:
        - কোভিড-১৯
    tag:
        - করোনা
        - IEDCR
theme:
    style: berlin
sidebar:
    enabled: true
comments:
    enabled: true
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
musicalbumenabled: false
productenabled: false
product:
    ratingValue: 2.5
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true
---

## IEDCR
* IEDCR- পূর্ণরুপ  কী?
= Institute of Epidemiology, Diseases Control and Research.
* IEDCR কত সালে প্রতিষ্ঠিত হয়?
= ১৯৭৬ সালে।
* IEDCR এর সদর দপ্তর কোথায়?
= মহাখালী, ঢাকা।
* IEDCR এর বর্তমান পরিচালক কে?
= প্রফেসর ড. মীরজাদী সাব্রিনা ফ্লোরা।
* PPE এর পূর্নরুপ কি?
= Personal protective Equipment

## Covid 19 

* কোভিড-১৯  রোগটি প্রথম কোথায় ও কবে সনাক্ত করা হয়?
= ডিসেম্বর ২০১৯ এ চীনের হুবেই প্রদেশের উহান নগরীতে রোগটি সনাক্ত করা হয়।
*  WHO, COVID-19 কবে PANDEMIC  হিসেবে ঘোষণা করে?
= ১১ মার্চ, ২০২০।
* COVID-19 রোগটির বহনকারী ভাইরাসটির নাম কি? 
= SARS COV 2 

## বাংলাদেশ ও কোভিড ১৯ 

* বাংলাদেশে প্রথম কোভিড-১৯ রোগী সনাক্ত করা হয় কবে?
= ৮ মার্চ, ২০২০
* কোভিড-১৯ রোগে আক্রান্ত হয়ে বাংলাদেশে প্রথম মারা যায় কবে?
= ১৮ মার্চ ২০২০