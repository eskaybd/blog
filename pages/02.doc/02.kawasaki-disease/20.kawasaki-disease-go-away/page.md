---
title: 'কাওয়াসাকি রোগ কি নিজে থেকে দূরে যেতে পারে?'
published: true
date: '14-06-2020 01:52'
publish_date: '14-06-2020 01:52'
metadata:
    'কাওয়াসাকি রোগ ': 'কাওয়াসাকি '
taxonomy:
    category:
        - 'কাওয়াসাকি রোগ'
        - docs
    tag:
        - 'কাওয়াসাকি প্রতিরোধ'
theme:
    style: berlin
visible: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookauthor: 'https://facebook.com/Eskaybd'
---

Your page content goes here.