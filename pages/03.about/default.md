---
title: 'লেখক '
media_order: FB_IMG_1586767464737.jpg
twitterenable: false
twittercardoptions: summary
facebookenable: true
route: about
---

![About Image](FB_IMG_1586767464737.jpg?cropResize=800,600)

</br>

নিজের সম্পর্কে বলার মতো তেমন কিছু নাই।  খুব সাধারন একটা মানুষ।  তবে মাঝে মাঝে আমার সহ ধর্মিণী আমাকে একটু কৃপণ বলে।  তার কথা টা সত্য হলেও হতে পারে। পরিবারের মানুষ গুলোকে খুব ভালোবাসি। 
</br>

[Facebook](https://www.facebook.com/eskaybd)

